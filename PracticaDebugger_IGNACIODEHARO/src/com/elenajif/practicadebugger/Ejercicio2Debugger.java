package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio2Debugger {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		// EL ERROR EST� EN QUE EN EL BUCLE COGEMOS DESDE EL N�MERO LE�DO EN TECLADO (EJEMPLO 4) Y VA DIVIDIENDO DESDE ESE MISMO N�MERO ENTRE SI MISMO HASTA 0, Y PUESTO QUE 
		// LA DIVISI�N ENTRE 0 NO TIENE SENTIDO, DA ERROR DE EXCEPCI�N
		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		/*
		 * SE ARREGLAR�A SIMPLEMENTE CAMBIANDO EL i>=0  A   I>0  Y ADEM�S HAY OTRO ERRORCILLO QUE EL RESULTADODIVISION DEBER�A SER UN LONG O DOUBLE PARA QUE DIESE EL RESULTAOD
		 * EXACTO DE LA DIVISI�N, YA QUE 4/3 NO DA 1, PERO TE LO MUESTRA AS� POR QUE EL INT NO ALMACENA
		 */
				
		lector.close();
	}

}
