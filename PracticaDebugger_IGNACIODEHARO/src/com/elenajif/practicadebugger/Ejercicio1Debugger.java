package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio1Debugger {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las
		 * variables
		 * 
		 */

		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;

		cantidadVocales = 0;
		cantidadCifras = 0;

		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		
		// EL ERROR DEL C�DIGO EST� EN LA SIGUIENTE L�NEA YA QUE LA OPERACI�N .LENGTH() TE DICE EL N�MERO DE LETRAS QUE TIENE UNA FRASE, EN EL EJEMPLO DE HOLA (4) PERO
		// A LA HORA DE HACER UN BUCLE QUE VAYA CARACTER A CARACTER TIENE QUE HACERSE CON .LENGTH-1 O CON EL < (y no <=)
		for (int i = 0; i <= cadenaLeida.length(); i++) {
			// habria que arreglar cambiar a i<= cadenaLeida.length()-1   o   i<cadenaLeida.length()
			caracter = cadenaLeida.charAt(i);
			if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
				cantidadVocales++;
			}
			// AQUI TAMBI�N HABRIA UN "ERROR" YA QUE SOLO PASAR� POR ESTE IF SI EL CARACTER ESTA POR ENCIMA DEL CHAR 0 O DEBAJO DEL CHAR 9
			if (caracter >= '0' && caracter <= '9') {
				cantidadCifras++;
			}
			
			// AQUI SER�A CAMBIAR LA CONDICI�N DEL IF A: 
			/*
			 * else if (caracter == ' '{
			 * 
			 * }
			 * else{
			 * 	canditadCifras++;
			 * }
			 * 
			 * Con este arreglo se contarian solo las cifras y no los espacios en una cadena, en caso de que se quisiera contar los espacios tambi�n se quitaria
			 * el else if
			 */
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);

		input.close();
	}
}
